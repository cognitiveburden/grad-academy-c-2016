﻿using System;

namespace GradAcademyCSharp.BusinessObjects
{
    public class Invoice
    {
        public int ClientId { get; set; }

        public int Id { get; set; }

        public decimal AmountDue { get; set; }

        public decimal TotalAmount { get; set; }

        public DateTime DueDate { get; set; }

        public bool Completed { get; set; }

        public DateTime? CompletedDate { get; set; }
    }
}
