﻿namespace GradAcademyCSharp.BusinessObjects
{
    public class Client
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public States State { get; set; }
    }
}
