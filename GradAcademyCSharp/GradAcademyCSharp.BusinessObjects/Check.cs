﻿using System;

namespace GradAcademyCSharp.BusinessObjects
{
    public class Check
    {
        public int Id { get; set; }

        public int CheckNumber { get; set; }

        public decimal Amount { get; set; }

        public int ClientId { get; set; }

        public string ClientName { get; set; }

        public int? AssignedWorker { get; set; }

        public bool Completed { get; set; }

        public DateTime? CompletedDate { get; set; }

        public DateTime? StartDate { get; set; }
    }
}
