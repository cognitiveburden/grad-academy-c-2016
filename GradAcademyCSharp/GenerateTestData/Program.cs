namespace GenerateTestData
{
	static class Program
	{
		static void Main(string[] args)
		{
			Run();
		}

		private static void Run()
		{
			GenerateCsvDataToFile.Generate(3);
			GenerateJsonDataToFile.Generate(3);

		}
	}
}
