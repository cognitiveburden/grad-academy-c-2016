using System.Collections.Generic;
using GradAcademyLoader.Domain;

namespace GenerateTestData
{
	public class Data
	{
		public IEnumerable<Client> Clients { get; set; }
		public IEnumerable<Check> Checks { get; set; }

		public Data(IEnumerable<Client> clients, IEnumerable<Check> checks)
		{
			Clients = clients;
			Checks = checks;
		}
	}
}