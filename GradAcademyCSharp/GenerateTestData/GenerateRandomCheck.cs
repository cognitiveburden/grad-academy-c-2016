using System;
using GradAcademyLoader.Domain;

namespace GenerateTestData
{
	public class GenerateRandomCheck
	{
		private readonly int _divisorToPickBadData;

		public GenerateRandomCheck() : this(1) { }

		public GenerateRandomCheck(int divisorToPickBadData)
		{
			_divisorToPickBadData = divisorToPickBadData;
		}

		public Check Generate(Client client)
		{
			var randomNum = Faker.RandomNumber.Next(0, 100);
			bool? randomBool = null;
			var checkNumber = Faker.RandomNumber.Next(1000, 100000);
			var checkAmount = Faker.RandomNumber.Next(10, 1000) * 1.123m;
			var clientReferenceId = client.Id;

			if (randomNum > 50)
				randomBool = randomNum % 2 == 0;

			if (randomNum % _divisorToPickBadData != 0)
			{
				checkNumber = Faker.RandomNumber.Next(-800, -100);
				checkAmount = Faker.RandomNumber.Next(-500, -1) * 1.1m;
				clientReferenceId = randomNum % 2 == 0 ? Guid.Empty : Guid.NewGuid();
			}

			return new Check(
								checkNumber,
								checkAmount,
								clientReferenceId,
								randomBool);
		}
	}
}