﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Process.Contracts;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Service
{
    public class PaymentService : IPaymentService
    {
        private readonly IFinancialDataAccess<Invoice> _invoiceDataAccess;
        private readonly IFinancialDataAccess<Check> _checkDataAccess;
        private readonly IPaymentDataAccess _paymentDataAccess;
        private readonly IFinancialDataAccess<PaidInvoice> _paidInvoiceDataAccess;

        public PaymentService(IFinancialDataAccess<Invoice> invoiceDataAccess, 
            IFinancialDataAccess<Check> checkDataAccess, 
            IPaymentDataAccess paymentDataAccess, IFinancialDataAccess<PaidInvoice> paidInvoiceDataAccess)
        {
            _invoiceDataAccess = invoiceDataAccess;
            _checkDataAccess = checkDataAccess;
            _paymentDataAccess = paymentDataAccess;
            _paidInvoiceDataAccess = paidInvoiceDataAccess;
        }

        public Check GetCheck(int checkId)
        {
            return _checkDataAccess.Get(checkId);
        }

        public IEnumerable<Invoice> GetOpenInvoices(int clientId)
        {
            return _invoiceDataAccess.Get()
               .Where(x => x.ClientId == clientId && !x.Completed).ToList();
        }

        public Invoice GetInvoice(int invoiceId)
        {
            return _invoiceDataAccess.Get(invoiceId);
        }

        public List<PaidInvoice> GetClosedInvoices(int workerId)
        {
            return
                _paidInvoiceDataAccess.Get()
                    .Where(x => x.WorkerId == workerId)
                    .OrderBy(x => x.DueDate)
                    .Select(AutoMapper.Mapper.Map<PaidInvoice>)
                    .ToList();
        }

        public decimal SchedulePayment(int checkId, int invoiceId)
        {
            var check = _checkDataAccess.Get(checkId);
            var invoice = _invoiceDataAccess.Get(invoiceId);
            if (check == null || invoice == null)
            {
                return 0;
            }

            var amountToPay = GetPaymentAmount(check, invoice);

            return ApplyPayment(check, invoice, amountToPay) ? amountToPay : 0;
        }

        private bool CanPayOffInvoice(Invoice invoice, decimal amount)
        {
            return amount >= invoice.AmountDue;
        }

        private bool CanPayOffCheck(Check check, decimal amount)
        {
            return amount >= check.Amount;
        }

        private decimal GetPaymentAmount(Check check, Invoice invoice)
        {
            return check.Amount > invoice.AmountDue ? invoice.AmountDue : check.Amount;
        }

        private bool ApplyPayment(Check check, Invoice invoice, decimal amount)
        {
            if (CanPayOffInvoice(invoice, amount))
            {
                invoice.Completed = true;
                invoice.CompletedDate = DateTime.Now;

                if (check.AssignedWorker != null)
                    _paidInvoiceDataAccess.Add(new PaidInvoice
                    {
                        InvoiceId = invoice.Id,
                        AmountDueBefore = invoice.AmountDue,
                        AmountPaid = amount,
                        WorkerId = check.AssignedWorker.Value,
                        DueDate = invoice.DueDate,
                        CompletedDate = DateTime.Now
                    });
            }

            if (CanPayOffCheck(check, amount))
            {
                check.Completed = true;
                check.CompletedDate = DateTime.Now;
            }

            invoice.AmountDue -= amount;
            check.Amount -= amount;

            return _paymentDataAccess.Update(check.Id, invoice.Id, check, invoice);
        }
    }
}
