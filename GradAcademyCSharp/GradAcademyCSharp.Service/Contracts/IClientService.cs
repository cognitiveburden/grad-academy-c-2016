﻿using System.Collections.Generic;
using GradAcademyCSharp.BusinessObjects;

namespace GradAcademyCSharp.Service.Contracts
{
    public interface IClientService
    {
        Client GetClient(int clientId);
        List<Client> GetClients();
        Client UpdateClientInformation(int id, Client updatedClient);
    }
}
