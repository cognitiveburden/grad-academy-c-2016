﻿using System.ComponentModel;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Process;
using GradAcademyCSharp.DataAccess.Process.Contracts;
using StructureMap;
using Container = StructureMap.Container;

namespace GradAcademyCSharp.Service
{
    public static class IoC
    {
        public static Container Configure(Container container)
        {
            container.Configure(c =>
            {
                c.For<IFinancialDataAccess<Worker>>().Use<WorkerDataAccess>();
                c.For<IFinancialDataAccess<Client>>().Use<ClientDataAccess>();
                c.For<IFinancialDataAccess<Check>>().Use<CheckDataAccess>();
                c.For<IFinancialDataAccess<Invoice>>().Use<InvoiceDataAccess>();
                c.For<IPaymentDataAccess>().Use<PaymentDataAccess>();
                c.For<IFinancialDataAccess<PaidInvoice>>().Use<PaidInvoiceDataAccess>();
            });

            return container;
        }
    }
}