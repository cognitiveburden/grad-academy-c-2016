﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faker;
using GradAcademyCSharp.BusinessObjects;

namespace DatabaseLoader
{
    public class DataFactory
    {
        private readonly List<Invoice> _invoices = new List<Invoice>(); 

        private List<Worker> _workers;
        public List<Worker> Workers
        {
            get { return _workers ?? (_workers = GetWorkers()); }
        }

        private List<Client> _clients;
        public List<Client> Clients
        {
            get { return _clients ?? (_clients = GetClients()); }
        }

        private List<Check> _checks;
        public List<Check> Checks
        {
            get { return _checks ?? (_checks = GetChecks()); }
        }

        public List<Invoice> GetInvoices(int clientId)
        {
            if (_invoices.Any(x => x.ClientId == clientId))
                return _invoices.Where(x => x.ClientId == clientId).ToList();

            var invoicesForClient = new List<Invoice>();

            for (var i = 0; i < RandomNumber.Next(8, 25); i++)
            {
                var amount = RandomNumber.Next(5000)*1.0m;
                invoicesForClient.Add(new Invoice
                {
                    ClientId = clientId,
                    Id = i + 1,
                    Completed = false,
                    AmountDue = amount,
                    TotalAmount = amount,
                    DueDate = DateTime.Now.AddDays(RandomNumber.Next(1, 60))
                });
            }

            _invoices.AddRange(invoicesForClient);

            return invoicesForClient;
        }

        private static List<Worker> GetWorkers()
        {
            var workers = new List<Worker>();
            for (var i = 0; i < 15; i++)
            {
                workers.Add(new Worker()
                {
                    FirstName = Name.First(),
                    LastName = Name.Last(),
                    JobTitle = "Account Analyst",
                    EmailAddress = String.Format("{0}.{1}@xyzTablets.com", Faker.Name.Last(), RandomNumber.Next(1, 40))
                });
            }

            return workers;
        }

        private List<Check> GetChecks()
        {
            var checks = new List<Check>();

            foreach (var client in Clients)
            {
                for (var i = 0; i < RandomNumber.Next(1, 5); i++)
                {
                    checks.Add(new Check
                    {
                        Id = i + 1,
                        Amount = RandomNumber.Next(20000) * 1.0m,
                        CheckNumber = RandomNumber.Next(100000),
                        ClientId = client.Id
                    });
                }
            }

            return checks;
        }

        private static List<Client> GetClients()
        {
            var clients = new List<Client>();
            for (var i = 0; i < 10; i++)
            {
                clients.Add(new Client
                {
                    Id = i + 1,
                    Name = Company.Name(),
                    State = (States) Enum.Parse(typeof(States), Address.UsStateAbbr())
                });
            }
            
            return clients;
        }
    }
}