﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.Models;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Controllers
{
    public class PaymentController : Controller
    {
        private readonly IPaymentService _paymentService;
        private readonly IWorkerService _workerService;

        private const int MAX_NUMBER_OF_CHECKS = 10;

        public PaymentController(IPaymentService paymentService, IWorkerService workerService)
        {
            _paymentService = paymentService;
            _workerService = workerService;
        }

        //GET
        public ActionResult Index(int id)
        {
            var workableChecks = _workerService.GetOpenWorkerChecks(id).Select(Mapper.Map<CheckModel>);
            var worker = SetWorkerStatistics(Mapper.Map<WorkerModel>(_workerService.GetWorker(id)));

            return View(new WorkerCheckModel
            {
                Worker = worker,
                WorkableChecks = workableChecks
            });
        }

        //GET
        public ActionResult Edit(int checkId)
        {
            var check = _paymentService.GetCheck(checkId);
            var clientInvoices = _paymentService.GetOpenInvoices(check.ClientId)
                .OrderBy(x => x.DueDate)
                .Select(Mapper.Map<Invoice, InvoiceModel>)
                .ToList();

            var viewModel = new PaymentModel()
            {
                Check = Mapper.Map<Check, CheckModel>(check),
                ClientInvoices = clientInvoices
            };

            return View(viewModel);
        }

        //POST
        [HttpPost]
        public ActionResult Edit(int checkId, List<InvoiceModel> invoices)
        {
            var check = Mapper.Map<CheckModel>(_paymentService.GetCheck(checkId));
            invoices = invoices.Where(x => x.Scheduled).OrderBy(x => x.DueDate).ToList();

            var paidInvoices = new List<PaidInvoiceModel>();

            foreach (var clientInvoice in invoices)
            {
                if (check.Amount == 0) break;

                var paymentAmount = _paymentService.SchedulePayment(checkId, clientInvoice.Id);
                if (paymentAmount == 0) continue;

                check.Amount -= paymentAmount;
                var paidInvoice = _paymentService.GetInvoice(clientInvoice.Id);

                paidInvoices.Add(new PaidInvoiceModel()
                {
                    InvoiceId = paidInvoice.Id,
                    AmountDueBefore =  paidInvoice.AmountDue + paymentAmount,
                    CompletedDate = DateTime.Now,
                    DueDate = paidInvoice.DueDate,
                    AmountPaid = paymentAmount
                });
            }

            return View("PaymentConfirmation", paidInvoices);
        }

        public ActionResult AddItems(int id)
        {
            var workerChecks = _workerService.GetOpenWorkerChecks(id).ToList();
            var newCheckCount = MAX_NUMBER_OF_CHECKS - workerChecks.Count();

            var newWorkerChecks = _workerService.GetMoreWorkerChecks(newCheckCount, id).ToList();
            workerChecks.AddRange(newWorkerChecks);

            var workableChecks = new List<CheckModel>();
            foreach (var workerCheck in workerChecks)
            {
                if (!workerCheck.AssignedWorker.HasValue)
                    continue;

                var checkModel = Mapper.Map<CheckModel>(workerCheck);

                workableChecks.Add(checkModel);
            }

            var worker = SetWorkerStatistics(Mapper.Map<WorkerModel>(_workerService.GetWorker(id)));

            return View("Index", new WorkerCheckModel
            {
                Worker = worker,
                WorkableChecks = workableChecks
            });
        }

        private WorkerModel SetWorkerStatistics(WorkerModel worker)
        {
            if (worker != null)
            {
                worker.AverageCompletionTime = _workerService.GetAverageCheckCompletionTime(worker.Id);
                worker.NumChecksCompleteInLastSevenDays = _workerService.GetNumberOfChecksCompleted(worker.Id, new TimeSpan(7, 0, 0, 0));
            }

            return worker;
        }

        public ActionResult BackToChecks()
        {
            var worker = _workerService.GetByEmail(User.Identity.Name);
            if (worker != null)
            {
                return RedirectToAction("Index", new { id = worker.Id });
            }

            return RedirectToAction("Login", "Home");
        }
    }
}