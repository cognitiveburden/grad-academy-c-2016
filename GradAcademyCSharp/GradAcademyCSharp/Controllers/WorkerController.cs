﻿using System.Web.Mvc;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.Models;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Controllers
{
    public class WorkerController : Controller
    {
        private readonly IWorkerService _workerService;

        public WorkerController(IWorkerService workerService)
        {
            _workerService = workerService;
        }

        public ActionResult Details(int id)
        {
            var workerModel = Mapper.Map<Worker, WorkerModel>(_workerService.GetWorker(id));

            return View(workerModel);
        }

        //Get: Worker/Edit/5
        public ActionResult Edit(int id)
        {
            var workerModel = Mapper.Map<Worker, WorkerModel>(_workerService.GetWorker(id));

            return View("Edit", workerModel);
        }

        //Post: Worker/Edit/5
        [HttpPost]
        public ActionResult Edit(WorkerModel worker)
        {
            if (!ModelState.IsValid)
            {
                return View(worker);
            }

            var updated = _workerService.SetProfileInformation(worker.Id, Mapper.Map<WorkerModel, Worker>(worker));

            if (updated)
            {
                return View("Details", worker);
            }

            return View(worker);
        }
    }
}
