﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using GradAcademyCSharp.Models;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Controllers
{
    public class PaidInvoiceController : Controller
    {
        private readonly IPaymentService _paymentService;

        public PaidInvoiceController(IPaymentService paymentService)
        {
            _paymentService = paymentService;
        }

        // GET: PaidInvoice
        public ActionResult Index(int id)
        {
            var paidInvoices = _paymentService.GetClosedInvoices(id).Select(Mapper.Map<PaidInvoiceModel>);

            return View(paidInvoices);
        }
    }
}
