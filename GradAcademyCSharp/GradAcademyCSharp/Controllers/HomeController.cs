﻿using System.Web.Mvc;
using GradAcademyCSharp.Models;
using GradAcademyCSharp.Service.Contracts;

namespace GradAcademyCSharp.Controllers
{
	public class HomeController : Controller
	{
		private readonly IWorkerService _workerService;

		public HomeController(IWorkerService workerService)
		{
			_workerService = workerService;
		}

		[HttpGet]
		public ActionResult Login()
		{
			if (!User.Identity.IsAuthenticated) return View("Index");

			var worker = _workerService.GetByEmail(User.Identity.Name);
			if (worker != null)
			{
				return RedirectToAction("Index", new {id = worker.Id});
			}

			return View("Index");
		}

		[HttpGet]
		public ActionResult Index(int id)
		{
			var worker = _workerService.GetWorker(id);
			
			return View(new WorkerModel
			{
				FirstName = worker.FirstName,
				Id = worker.Id,
				JobTitle = worker.JobTitle,
				LastName = worker.LastName,
				EmailAddress = worker.EmailAddress
			});
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your application description page.";

			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";

			return View();
		}
	}
}