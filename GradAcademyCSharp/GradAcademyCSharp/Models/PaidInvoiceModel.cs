﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GradAcademyCSharp.Models
{
    public class PaidInvoiceModel
    {
        public int Id { get; set; }

        [DataType(DataType.Currency)]
        [DisplayName("Amount Paid")]
        public decimal AmountPaid { get; set; }

       [DisplayName("Invoice Id")]
       public int InvoiceId { get; set; }

       [DisplayName("Amount Due")]
       [DataType(DataType.Currency)]
       public decimal AmountDueAfter
        {
            get
            {
                return AmountDueBefore - AmountPaid; 
            }
        }

       [DisplayName("Total Amount")]
       [DataType(DataType.Currency)]
       public decimal AmountDueBefore { get; set; }

       [DisplayName("Due Date")]
       public DateTime DueDate { get; set; }

       [DisplayName("Completed Date")]
       public DateTime CompletedDate { get; set; }
    }
}