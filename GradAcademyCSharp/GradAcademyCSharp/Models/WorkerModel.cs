﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GradAcademyCSharp.Models
{
    public class WorkerModel
    {
        public int Id { get; set; }

        [Required]
        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [Required]
        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Job Title")]
        public string JobTitle { get; set; }

        [Required]
        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [DisplayName("Number of checks completed (last 7 days): ")]
        public int NumChecksCompleteInLastSevenDays { get; set; }

        [DisplayName("Average check completion time: ")]
        [DisplayFormat(DataFormatString="{0:hh\\:mm}")]
        public TimeSpan AverageCompletionTime { get; set; }
    }
}