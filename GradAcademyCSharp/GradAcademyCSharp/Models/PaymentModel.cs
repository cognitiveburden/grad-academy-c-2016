﻿using System.Collections.Generic;

namespace GradAcademyCSharp.Models
{
    public class PaymentModel
    {
        public CheckModel Check { get; set; }

        public List<InvoiceModel> ClientInvoices { get; set; }
    }
}