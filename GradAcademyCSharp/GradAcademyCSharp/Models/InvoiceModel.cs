﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GradAcademyCSharp.Models
{
    public class InvoiceModel
    {
        [DisplayName("Client Id")]
        public int ClientId { get; set; }

        [DisplayName("Invoice Id")]
        public int Id { get; set; }

        [DisplayName("Amount Due")]
        [DataType(DataType.Currency)]
        public decimal AmountDue { get; set; }

        [DisplayName("Total Amount")]
        [DataType(DataType.Currency)]
        public decimal TotalAmount { get; set; }

        [DisplayName("Due Date")]
        public DateTime DueDate { get; set; }

        public bool Completed { get; set; }

        [DisplayName("Completed Date")]
        public DateTime CompletedDate { get; set; }

        [DisplayName("Schedule")]
        public bool Scheduled { get; set; }
    }
}