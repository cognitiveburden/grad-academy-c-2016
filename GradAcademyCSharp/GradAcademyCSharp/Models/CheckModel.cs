﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace GradAcademyCSharp.Models
{
    public class CheckModel
    {
        public int Id { get; set; }

        [DisplayName("Check Number")]
        public int CheckNumber { get; set; }

        [DataType(DataType.Currency)]
        public decimal Amount { get; set; }

        public int ClientId { get; set; }

        [Required]
        [DisplayName("Client")]
        public string ClientName { get; set; }

        public int? AssignedWorker { get; set; }

        public bool Completed { get; set; }

        [DisplayName("Completed Date")]
        public DateTime? CompletedDate { get; set; }

        [DisplayName("Start Date")]
        public DateTime? StartDate { get; set; }
    }
}