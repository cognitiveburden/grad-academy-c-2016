﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GradAcademyCSharp.Startup))]
namespace GradAcademyCSharp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
