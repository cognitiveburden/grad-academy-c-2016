﻿using GradAcademyLoader.Domain;

namespace GradAcademyLoader.Tests
{
	internal class FakeBadConfig : ILoaderConfig
	{
		public string SourceDirectory { get { return "BadDir"; } }
		public string CsvClientsFileName { get { return "BadName"; } }
		public string CsvChecksFileName { get { return "BadName"; } }
		public string JsonClientsFileName { get { return "BadName"; } }
		public string JsonChecksFileName { get { return "BadName"; } }
	}
}