using System;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GradAcademyLoader.Tests
{
	/// <summary>
	/// Summary description for WhenParsingAClient
	/// </summary>
	[TestClass]
	public class WhenParsingAClient
	{
		private const string ValidId = "0192f50d-871c-46f1-af02-2c757953e696";
		private const string ValidName = "CompanyName";
		private const string ValidState = "VA";
		private Func<string, string, string, ParsedClient> _sut;

		[TestInitialize]
		public void StartBeforeEachTest()
		{
			_sut = ClientParser.ParseClient;
		}

		[TestCleanup]
		public void CleanUpAfterEachTest()
		{
			_sut = null;
		}

		[TestMethod]
		public void GivenInvalidIdThenGetErrorOnClientId()
		{
			const string invalidId = "448ad";
			var parsedClient = _sut(invalidId, ValidName, ValidState);
			Assert.IsTrue(parsedClient.ParsingErrors.Contains("Client Id"));
		}

		[TestMethod]
		public void GivenValidInformationThenClientIsNotNull()
		{
			var result = _sut(ValidId, ValidName, ValidState);
			Assert.IsNotNull(result);
		}

		[TestMethod]
		public void GivenUnknownStateThenParseErrorContainsState()
		{
			var result = _sut(ValidId, ValidName, "ZZ");
			StringAssert.Contains(result.ParsingErrors,"State");
		}

		[TestMethod]
		public void GivenEmptyNameThenParseErrorContainsName()
		{
			var result = _sut(ValidId, string.Empty, ValidState);
			StringAssert.Contains(result.ParsingErrors,"Name");
		}

		[TestMethod]
		public void GivenInvalidNameThenParseErrorContainsName()
		{
			var result = _sut(ValidId, "J1mmi3", ValidState);
			StringAssert.Contains(result.ParsingErrors,"Name");
		}
	}
}
