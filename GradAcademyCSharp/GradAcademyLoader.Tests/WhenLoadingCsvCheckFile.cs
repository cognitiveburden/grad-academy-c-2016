﻿using System;
using System.IO;
using System.Linq;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace GradAcademyLoader.Tests
{

	[TestClass]
	[DeploymentItem(@"Data\")]
	public class WhenLoadingCsvCheckFile
	{
		private ILoaderConfig _fakeConfig;

		[TestInitialize]
		public void TestInitialize()
		{
			_fakeConfig = MockRepository.GenerateStub<ILoaderConfig>();
			_fakeConfig.Stub(x => x.SourceDirectory).Return(AppDomain.CurrentDomain.BaseDirectory);
		}

		[TestCleanup]
		public void TestCleanUp()
		{
			_fakeConfig = null;
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void GivenAnInvalidFileNameThenException()
		{
			var sut = new CheckCsvDataProvider(new FakeBadConfig());
			sut.Read();
			Assert.Fail("Should Have thrown Exception");
		}


		[TestMethod]
		public void GivenAValidFileWithOneRecordThenSkipHeaderRow()
		{
			_fakeConfig.Stub(x => x.CsvChecksFileName).Return("One_Check.csv");

			var sut = new CheckCsvDataProvider(_fakeConfig);
			var result = sut.Read();

			Assert.AreEqual(1, result.Count());
		}

		[TestMethod]
		public void GivenABadSepartorFileWithOneRecordThenOnlyGetOneRowBack()
		{
			_fakeConfig.Stub(x => x.CsvChecksFileName).Return("BadSeparator_Check.csv");

			var sut = new CheckCsvDataProvider(_fakeConfig);
			var result = sut.Read();

			StringAssert.Contains(result.First().Errors, "correct number of columns");
		}

		[TestMethod]
		public void GivenAGoodFileWithOneRecordThenFourRowsBack()
		{
			_fakeConfig.Stub(x => x.CsvChecksFileName).Return("One_Check.csv");

			var sut = new CheckCsvDataProvider(_fakeConfig);
			var result = sut.Read();

			Assert.IsNotNull(result.First());
		}

		[TestMethod]
		[ExpectedException(typeof(CheckFileReadingException))]
		public void GivenFileLockedThenThrowCheckFileReadingException()
		{
			var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
			var fileName = "Locked_Check.csv";
			_fakeConfig.Stub(x => x.SourceDirectory).Return(baseDirectory);
			_fakeConfig.Stub(x => x.CsvChecksFileName).Return(fileName);

			var lockingPath = Path.Combine(baseDirectory, fileName);

			FileStream s = File.Open(lockingPath, FileMode.Open, FileAccess.ReadWrite);

			var sut = new CheckCsvDataProvider(_fakeConfig);
			var result = sut.Read();
			Assert.Fail("Should Have thrown check file reading exception;");
		}
	}
}
