﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace GradAcademyLoader.Tests
{
	[TestClass]
	public class WhenReadingClients
	{
		private IClientDataProvider _clientDataProvider;
		private readonly string _knownClientId = "44a7bee6-5207-4934-9622-e4a66117f8ed";
		private readonly string _knownClientName = "Jim";
		private readonly string _knownClientState = "CO";
		private DataLoader _sut;

		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//	_clientDataProvider = MockRepository.GenerateStub<IClientDataProvider>();
		//	_clientDataProvider.Stub(x => x.Read()).Return(new List<string[]> { new[] { _knownClientId, _knownClientName, _knownClientState } });
		//	_sut = new DataLoader(_clientDataProvider, MockRepository.GenerateStub<ICheckDataProvider>());
		//}

		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//	_clientDataProvider = null;
		//	_sut = null;
		//}

		//[TestMethod]
		//public void GivenAClientThenKnownClientIdReturned()
		//{
		//	var clients = _sut.ReadClients();
		//	Assert.AreEqual(clients.First().Id, Guid.Parse(_knownClientId));
		//}

		//[TestMethod]
		//public void GivenAClientThenKnownNameReturned()
		//{
		//	var clients = _sut.ReadClients();
		//	Assert.AreEqual(clients.First().Name, _knownClientName);
		//}

		//[TestMethod]
		//public void GivenAClientThenKnownStateReturned()
		//{
		//	var clients = _sut.ReadClients();
		//	Assert.AreEqual(clients.First().State, Enum.Parse(typeof(States), _knownClientState));
		//}
	}
}
