﻿using System;
using System.Linq;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace GradAcademyLoader.Tests
{
	[TestClass]
	[DeploymentItem(@"Data\")]
	public class WhenLoadingJsonCheckFile
	{
		private ILoaderConfig _fakeConfig;

		[TestInitialize]
		public void TestInitialize()
		{
			_fakeConfig = MockRepository.GenerateStub<ILoaderConfig>();
			_fakeConfig.Stub(x => x.SourceDirectory).Return(AppDomain.CurrentDomain.BaseDirectory);
		}

		[TestCleanup]
		public void TestCleanUp()
		{
			_fakeConfig = null;
		}

		[TestMethod]
		public void GivenASingleGoodCheckThenParsedIntoACheckItem()
		{
			_fakeConfig.Stub(x => x.JsonChecksFileName).Return("One_Check.json");
			var sut = new CheckJsonNetDataProvider(_fakeConfig);

			var result = sut.Read();

			Assert.IsTrue(IsValidCheck(result.First()));
		}

		[TestMethod]
		public void GivenABadCheckThenParsedWithErrors()
		{
			_fakeConfig.Stub(x => x.JsonChecksFileName).Return("Bad_Check.json");
			var sut = new CheckJsonNetDataProvider(_fakeConfig);

			var result = sut.Read();
			Assert.IsFalse(IsValidCheck(result.First()));
		}


		private bool IsValidCheck(CheckLine check)
		{
			if (check == null)
				return false;
			if (check.SourceCheck == null)
				return false;

			return !check.HasErrors;
		}
	}
}
