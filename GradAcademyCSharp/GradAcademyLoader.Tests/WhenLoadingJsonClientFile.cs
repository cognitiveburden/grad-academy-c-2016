﻿using System;
using System.Linq;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Rhino.Mocks;

namespace GradAcademyLoader.Tests
{
	[TestClass]
	[DeploymentItem(@"Data\")]
	public class WhenLoadingJsonClientFile
	{
		private ILoaderConfig _fakeConfig;

		[TestInitialize]
		public void TestInitialize()
		{
			_fakeConfig = MockRepository.GenerateStub<ILoaderConfig>();
			_fakeConfig.Stub(x => x.SourceDirectory).Return(AppDomain.CurrentDomain.BaseDirectory);
		}

		[TestMethod]
		public void GivenAValidClientFileWithOneClientThenGetOneValidClient()
		{
			_fakeConfig.Stub(x => x.JsonClientsFileName).Return("One_Client.json");

			var sut = new ClientJsonNetDataProvider(_fakeConfig);

			var result = sut.Read();

			Assert.IsTrue(IsValidClient(result.First()));
		}
		
		[TestMethod]
		public void GivenABadClientFileThenGetClienWithErrors ()
		{
			_fakeConfig.Stub(x => x.JsonClientsFileName).Return("Bad_Client.json");

			var sut = new ClientJsonNetDataProvider(_fakeConfig);

			var result = sut.Read();

			Assert.IsFalse(IsValidClient(result.First()));
		}


		private bool IsValidClient(ClientLine client)
		{
			if (client == null)
				return false;
			if (client.SourceClient == null)
				return false;

			return !client.HasErrors;
		}

	}
}
