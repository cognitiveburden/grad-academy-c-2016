﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradAcademyCSharp.DataAccess.Models
{
    public class CheckEntity
    {
        [Key]
        public int Id { get; set; }

        public int CheckNumber { get; set; }

        [Column(TypeName="Money")]
        public decimal Amount { get; set; }

        [ForeignKey("ClientEntity")]
        public int ClientId { get; set; }

        public virtual ClientEntity ClientEntity { get; set; }

        public int? AssignedWorker { get; set; }

        public bool Completed { get; set; }

        public DateTime? CompletedDate { get; set; }

        public DateTime? StartDate { get; set; }

    }
}
