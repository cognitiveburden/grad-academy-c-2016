﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradAcademyCSharp.DataAccess.Models
{
    public class PaidInvoiceEntity
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName = "Money")]
        public decimal AmountPaid { get; set; }

        public decimal AmountDueBefore { get; set; }

        public DateTime DueDate { get; set; }

        [ForeignKey("InvoiceEntity")]
        public int InvoiceId { get; set; }

        public virtual InvoiceEntity InvoiceEntity { get; set; }

        [ForeignKey("WorkerEntity")]
        public int WorkerId { get; set; }

        public virtual WorkerEntity WorkerEntity { get; set; }

        public DateTime CompletedDate { get; set; }
    }
}
