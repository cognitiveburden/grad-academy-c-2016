﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradAcademyCSharp.DataAccess.Models
{
    public class InvoiceEntity
    {

        [Key]
        public int Id { get; set; }

        public virtual ClientEntity ClientEntity { get; set; }

        [ForeignKey("ClientEntity")]
        public int ClientId { get; set; }

        [Column(TypeName = "Money")]
        public decimal AmountDue { get; set; }

        [Column(TypeName = "Money")]
        public decimal TotalAmount { get; set; }

        public DateTime DueDate { get; set; }

        public bool Completed { get; set; }

        public DateTime? CompletedDate { get; set; }
    }
}
