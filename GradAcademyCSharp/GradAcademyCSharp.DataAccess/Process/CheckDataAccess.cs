﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using GradAcademyCSharp.BusinessObjects;
using GradAcademyCSharp.DataAccess.Models;
using GradAcademyCSharp.DataAccess.Process.Contracts;

namespace GradAcademyCSharp.DataAccess.Process
{
    public class CheckDataAccess : IFinancialDataAccess<Check>
    {
        public Check Add(Check newItem)
        {

            CheckEntity newCheck;
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    newCheck = context.Checks.Add(Mapper.Map<CheckEntity>(newItem));
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                return null;
            }

            return Mapper.Map<Check>(newCheck);
        }

        public bool Remove(int id)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var check = context.Checks.FirstOrDefault(x => x.Id == id);
                    if (check == null)
                        return false;

                    context.Checks.Remove(check);
                    context.SaveChanges();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public Check Update(int id, Check updatedItem)
        {
            try
            {
                using (var context = new FinancialsDataContext())
                {
                    var check = context.Checks.FirstOrDefault(x => x.Id == id);
                    if (check == null)
                        return null;

                    Mapper.Map(updatedItem, check);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return null;
            }

            return updatedItem;
        }

        public Check Get(int id)
        {
            using (var context = new FinancialsDataContext())
            {
                return Mapper.Map<Check>(context.Checks.FirstOrDefault(x => x.Id == id));
            }
        }

        public IList<Check> Get()
        {
            using (var context = new FinancialsDataContext())
            {
                return context.Checks.Select(Mapper.Map<Check>).ToList();
            }
        }
    }
}