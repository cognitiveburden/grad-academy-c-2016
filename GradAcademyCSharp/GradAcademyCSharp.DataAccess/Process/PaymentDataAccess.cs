﻿﻿using System;
﻿using System.Diagnostics;
﻿using System.Linq;
﻿using AutoMapper;
﻿using GradAcademyCSharp.BusinessObjects;
﻿using GradAcademyCSharp.DataAccess.Process.Contracts;

namespace GradAcademyCSharp.DataAccess.Process
{
    public class PaymentDataAccess : IPaymentDataAccess
    {
        public bool Update(int checkId, int invoiceId, Check updatedCheck, Invoice updatedInvoice)
        {
            using (var context = new FinancialsDataContext())
            {
                var check = context.Checks.FirstOrDefault(x => x.Id == checkId);
                if (check == null)
                    return false;

                var invoice = context.Invoices.FirstOrDefault(x => x.Id == invoiceId);
                if (invoice == null)
                    return false;

                try
                {
                    Mapper.Map(updatedCheck, check);
                    Mapper.Map(updatedInvoice, invoice);

                    context.SaveChanges();

                    return true;

                }
                catch (Exception ex)
                {
                    Trace.TraceError(ex.Message);
                    Trace.TraceError(ex.StackTrace);

                    return false;
                }
            }
        }
    }
}
