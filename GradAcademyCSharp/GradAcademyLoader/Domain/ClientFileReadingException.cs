using System;

namespace GradAcademyLoader.Domain
{
	internal class ClientFileReadingException : Exception
	{
		public ClientFileReadingException(string message, Exception innerException)
			: base(message, innerException)
		{

		}
	}
}