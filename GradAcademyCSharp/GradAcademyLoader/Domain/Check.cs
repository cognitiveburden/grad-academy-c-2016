﻿using System;
using System.Runtime.Serialization;

namespace GradAcademyLoader.Domain
{
	[DataContract]
	public class Check
	{
		public Check(int number, decimal amount, Guid clientId, bool? hasImage)
		{
			Number = number;
			Amount = amount;
			ClientId = clientId;
			HasImage = hasImage;
		}

		[DataMember]
		public int Number { get; private set; }
		[DataMember]
		public decimal Amount { get; private set; }
		[DataMember]
		public Guid ClientId { get; private set; }
		[DataMember]
		public bool? HasImage { get; private set; }

		public override string ToString()
		{
			return "Check #: " + Number + " Client " + ClientId + " Amount " + Amount;
			//TODO: Make this simpler to allow new grads to fix like this:
			//StringBuilder toStringBuilder = new StringBuilder();
			//toStringBuilder.AppendFormat("Check Number: '{0}';", Number).AppendLine();
			//toStringBuilder.AppendFormat("For Amount: '{0}';", Amount).AppendLine();
			//toStringBuilder.AppendFormat("For Client: '{0}';", ClientId).AppendLine();
			//toStringBuilder.AppendFormat("For HasImage: '{0}';", HasImage).AppendLine();
			//return toStringBuilder.ToString();
		}
	}
}
