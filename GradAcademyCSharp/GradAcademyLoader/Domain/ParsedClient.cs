﻿namespace GradAcademyLoader.Domain
{
	public class ParsedClient:Client
	{
		public string ParsingErrors { get; private set; }

		public ParsedClient(Client sourceClient, string parsingErrors)
			:base(sourceClient.Id, sourceClient.Name, sourceClient.State)
		{
			ParsingErrors = parsingErrors;
		}
	}
}