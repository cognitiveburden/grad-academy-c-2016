using System;

namespace GradAcademyLoader.Domain
{
	internal class CheckFileReadingException : Exception
	{
		public CheckFileReadingException(string message, Exception innerException)
			: base(message, innerException)
		{

		}

	}
}