using System;
using System.Text;

namespace GradAcademyLoader.Domain
{
	public class LineInformation
	{
		public string FileName { get; private set; }
		public int LineNumber { get; private set; }
		public string Rawstring { get; private set; }
		public DateTime DateParsed { get; private set; }

		public LineInformation(string fileName, int lineNumber, string rawString) :
			this(fileName, lineNumber, rawString, DateTime.Now) { }

		public LineInformation(string fileName, int lineNumber, string rawstring, DateTime dateParsed)
		{
			FileName = fileName;
			LineNumber = lineNumber;
			Rawstring = rawstring;
			DateParsed = dateParsed;
		}

		public override string ToString()
		{
			StringBuilder sb = new StringBuilder();
			sb.AppendFormat("File:{0};", FileName);
			sb.AppendLine();
			sb.AppendFormat("Line:{0}; Parsed On:{1}", LineNumber, DateParsed);
			sb.AppendLine();
			sb.AppendLine("Raw Line:");
			sb.AppendLine(Rawstring);
			return sb.ToString();
		}
	}
}