﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;

namespace GradAcademyLoader.DataAccess
{
	public class CheckJsonDataProvider:ICheckDataProvider
	{
		private readonly ILoaderConfig _loaderConfig;

		public CheckJsonDataProvider(ILoaderConfig loaderConfig)
		{
			_loaderConfig = loaderConfig;
		}

		public IEnumerable<CheckLine> Read()
		{
			if (!File.Exists(Source))
				throw new ArgumentException("Error could not find Check Json file at: " + Source);

			var checks = ReadChecksFromJsonFile(Source);

			return checks;
		}

		private IEnumerable<CheckLine> ReadChecksFromJsonFile(string filePath)
		{
			var checkLines = new List<CheckLine>();
			var checks = new List<Check>();

			using (var stream = File.OpenRead(filePath))
			{
				var jsonSerializer = new DataContractJsonSerializer(typeof(List<Check>));

				checks = jsonSerializer.ReadObject(stream) as List<Check>;
			}

			for (var i = 0; i < checks.Count; i++)
			{
				checkLines.Add(new CheckLine(
					new LineInformation(filePath, i + 1, string.Empty),
					checks[i],
					string.Empty));
			}
			return checkLines ;	
		}

		public string Source { get { return Path.Combine(_loaderConfig.SourceDirectory, _loaderConfig.JsonChecksFileName); } }
	}
}
