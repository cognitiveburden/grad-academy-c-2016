﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;

namespace GradAcademyLoader.DataAccess
{
	public class ClientJsonDataProvider : IClientDataProvider
	{
		private readonly ILoaderConfig _loaderConfig;

		public ClientJsonDataProvider(ILoaderConfig loaderConfig)
		{
			_loaderConfig = loaderConfig;
		}

		public IEnumerable<ClientLine> Read()
		{
			if (!File.Exists(Source))
				throw new ArgumentException("Error could not find Client Json file at: " + Source);

			var clients = ReadClientsFromJsonFile(Source);

			return clients;
		}

		private IEnumerable<ClientLine> ReadClientsFromJsonFile(string filePath)
		{
			var clientLines = new List<ClientLine>();
			var clients = new List<Client>();

			using (var stream = File.OpenRead(filePath))
			{
				var jsonSerializer = new DataContractJsonSerializer(typeof(List<Client>));

				clients = jsonSerializer.ReadObject(stream) as List<Client>;
			}

			for (var i = 0; i < clients.Count; i++)
			{
				clientLines.Add(new ClientLine(
					new LineInformation(filePath, i + 1, string.Empty),
					clients[i],
					string.Empty));
			}
			return clientLines;
		}

		public string Source { get { return Path.Combine(_loaderConfig.SourceDirectory, _loaderConfig.JsonClientsFileName); } }
	}
}