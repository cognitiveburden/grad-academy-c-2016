﻿using System;
using System.Collections.Generic;
using System.IO;
using CsvHelper;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;

namespace GradAcademyLoader.DataAccess
{
	public class ClientCsvDataProvider : IClientDataProvider
	{
		private readonly ILoaderConfig _config;

		public ClientCsvDataProvider(ILoaderConfig config)
		{
			_config = config;
		}

		public IEnumerable<ClientLine> Read()
		{
			if (!File.Exists(Source))
				throw new ArgumentException("Error the file path:" + Source + " does not exist.");

			var clientLines = ReadClientsFromFile(Source);
			//TODO: Ensure Client Exists
			return clientLines;
		}

		private static IEnumerable<ClientLine> ReadClientsFromFile(string filePath)
		{
			var clientLines = new List<ClientLine>();
			try
			{
				using (var textReader = File.OpenText(filePath))
				{
					using (var parser = new CsvParser(textReader))
					{
						parser.Read(); //Read Header
						while (true)
						{
							var row = parser.Read();
							if (row == null)
								break;

							var lineInfo = new LineInformation(filePath, parser.RawRow, parser.RawRecord, DateTime.Now);

							var tempClient = 
								row.Length != 3 
								? new ParsedClient(new Client(Guid.Empty, string.Empty,States.NA), "Error the line was not parsed into correct number of columns.") 
								: ClientParser.ParseClient(row[0], row[1], row[2]);

							clientLines.Add(new ClientLine(lineInfo, tempClient, tempClient.ParsingErrors));
						}
					}
				}
			}
			catch (IOException io)
			{
				throw new ClientFileReadingException("Error trying to read from Client File.", io);
			}
			return clientLines;
		}

		public string Source { get { return Path.Combine(_config.SourceDirectory, _config.CsvClientsFileName); } }
	}
}
