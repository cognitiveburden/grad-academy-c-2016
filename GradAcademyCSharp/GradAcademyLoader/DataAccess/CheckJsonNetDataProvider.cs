using System;
using System.Collections.Generic;
using System.IO;
using GradAcademyLoader.Domain;
using GradAcademyLoader.Service;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GradAcademyLoader.DataAccess
{
	public class CheckJsonNetDataProvider : ICheckDataProvider
	{
		private readonly ILoaderConfig _loaderConfig;

		public CheckJsonNetDataProvider(ILoaderConfig loaderConfig)
		{
			_loaderConfig = loaderConfig;
		}

		public IEnumerable<CheckLine> Read()
		{
			if (!File.Exists(Source))
				throw new ArgumentException("Error could not find Check Json file at: " + Source);

			var checks = ReadChecksFromJsonFile(Source);

			return checks;
		}

		private static IEnumerable<CheckLine> ReadChecksFromJsonFile(string filePath)
		{
			var checkLines = new List<CheckLine>();
			using (var reader = File.OpenText(filePath))
			{
				using (var r = new JsonTextReader(reader))
				{
					while (r.Read())
					{
						if (r.TokenType != JsonToken.StartObject) continue;

						var jsonObject = JObject.Load(r);

						var rawJsonString = jsonObject.ToString();
						var lineNumber = r.LineNumber;
						var error = string.Empty;

						var check = JsonConvert.DeserializeObject<CheckJsonNetDataProvider.JsonCheck>(rawJsonString,
							new JsonSerializerSettings()
							{
								Error = (sender, args) =>
								{
									error = args.ErrorContext.Error.Message;
									args.ErrorContext.Handled = true;
								}
							});

						var lineInfo = new LineInformation(filePath, lineNumber, rawJsonString);
						if (check == null)
						{
							checkLines.Add(new CheckLine(lineInfo, null, error));
						}
						else
						{
							var parsedCheck = CheckParser.ParseCheck(check.Number, check.Amount, check.ClientId, check.HasImage ?? string.Empty);
							checkLines.Add(new CheckLine(lineInfo, parsedCheck.SourceCheck, parsedCheck.ParsingErrors + error));
						}
					}
				}
			}
			return checkLines;
		}

		public string Source { get { return Path.Combine(_loaderConfig.SourceDirectory, _loaderConfig.JsonChecksFileName); } }

		private class JsonCheck
		{
			public string Number { get; set; }
			public string Amount { get; set; }
			public string ClientId { get; set; }
			public string HasImage { get; set; }
		}
	}
}