using System;
using System.Text;
using System.Text.RegularExpressions;
using GradAcademyLoader.Domain;

namespace GradAcademyLoader.Service
{
	internal static class CheckParser
	{
		internal static ParsedCheck ParseCheck(string number, string amount, string clientId, string hasImage)
		{

			var errorMessages = new StringBuilder();
			var tempNumber = -1;
			var tempAmount = decimal.MinValue;
			var tempClientId = Guid.Empty;
			bool? tempHasImage = false;

			if (!IsValidNumber(number) || !Int32.TryParse(number, out tempNumber))
			{
				errorMessages.AppendLine(string.Format(" The Check Number '{0}' is not valid. ", number));
			}

			if (!IsValidAmount(amount) || !Decimal.TryParse(amount, out tempAmount))
			{
				errorMessages.AppendLine(string.Format(" The Check Amount '{0}' is not valid. ", amount));
			}

			if (!IsValidClientId(clientId) || !Guid.TryParse(clientId, out tempClientId))
			{
				errorMessages.AppendLine(string.Format(" The Check Client Id '{0}' is not valid. ", clientId));
			}

			if (!IsValidHasImage(hasImage) || !TryParseNullableBool(hasImage, out tempHasImage))
			{
				errorMessages.AppendLine(string.Format(" The check with clientId '{0}' does not havea valid Image. ", clientId));
			}

			return new ParsedCheck(new Check(tempNumber, tempAmount, tempClientId, tempHasImage), errorMessages.ToString());
		}


		private static bool IsValidHasImage(string hasImage)
		{
			return Regex.IsMatch(hasImage, @"^(true|false)$|^$", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
		}

		private static bool IsValidClientId(string clientId)
		{
			var matchesFormat = Regex.IsMatch(clientId, @"\b[A-F0-9]{8}(?:-[A-F0-9]{4}){3}-[A-F0-9]{12}\b", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
			var isEmptyGuid = Guid.Empty.ToString().Equals(clientId);

			return matchesFormat && !isEmptyGuid;
		}

		private static bool IsValidAmount(string amount)
		{
			return Regex.IsMatch(amount, @"^\d+(\.\d{1,2})?", RegexOptions.IgnorePatternWhitespace);
		}

		private static bool IsValidNumber(string number)
		{
			return Regex.IsMatch(number, @"^\d+$", RegexOptions.IgnorePatternWhitespace);
		}

		private static bool TryParseNullableBool(string input, out bool? result)
		{
			bool temp;
			result = null;

			if (Boolean.TryParse(input, out temp))
			{
				result = temp;
				return true;
			}
			else if (string.IsNullOrWhiteSpace(input))
				return true;
			else
				return false;
		}
	}
}