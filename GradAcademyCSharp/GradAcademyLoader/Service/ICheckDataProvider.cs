using System.Collections.Generic;
using GradAcademyLoader.Domain;

namespace GradAcademyLoader.Service
{
	public interface ICheckDataProvider
	{
		IEnumerable<CheckLine> Read();
		string Source { get; }
	}
}