using System.Collections.Generic;
using GradAcademyLoader.Domain;

namespace GradAcademyLoader.Service
{
	public interface IClientDataProvider
	{
		IEnumerable<ClientLine> Read();
		string Source { get; }
	}
}