using System;
using System.Collections.Generic;
using System.Text;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;

namespace GradAcademyLoader.Service
{
	public class DataLoader
	{
		private readonly IClientDataProvider _clientDataProvider;
		private readonly ICheckDataProvider _checkDataProvider;

		public event Action<Client> ClientCreated;
		public event Action<Check> CheckCreated;

		public DataLoader(IClientDataProvider clientDataProvider, ICheckDataProvider checkDataProvider)
		{
			_clientDataProvider = clientDataProvider;
			_checkDataProvider = checkDataProvider;
		}

		public IEnumerable<CheckLine> ReadChecks()
		{
			return _checkDataProvider.Read();
		}

		public IEnumerable<ClientLine> ReadClients()
		{
			return _clientDataProvider.Read();
		}

	}

}