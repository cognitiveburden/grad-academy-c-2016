﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GradAcademyLoader.DataAccess;
using GradAcademyLoader.Domain;
using Newtonsoft.Json;

namespace GradAcademyLoader.Service
{
	public class EtlDataLoader
	{
		private readonly IClientLoader _clientLoader;
		private readonly ICheckLoader _checkLoader;
		private readonly IEtlRepository _etlRepository;

		public EtlDataLoader(IClientLoader clientLoader, ICheckLoader checkLoader, IEtlRepository etlRepository)
		{
			_clientLoader = clientLoader;
			_checkLoader = checkLoader;
			_etlRepository = etlRepository;
		}

		public void LoadData()
		{
			LoadClients();
			LoadChecks();
		}

		private void LoadChecks()
		{
			var checks = _checkLoader.ReadChecks();
			//TODO: Business Validation for  Checks
			_etlRepository.SaveChecks(checks.Where(c=>!c.HasErrors).Select(x=>x.SourceCheck));
		}

		private void LoadClients()
		{
			var clients = _clientLoader.ReadClients();
			//TODO: Business Validation Clients
			_etlRepository.SaveClients(clients.Where(c=>!c.HasErrors).Select(x=>x.SourceClient));
		}
	}
}
